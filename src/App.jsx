import { Canvas, useLoader } from '@react-three/fiber'
import { OrbitControls, Stars } from "@react-three/drei";
import { Physics, usePlane, useBox } from "@react-three/cannon";
import { TextureLoader } from 'three'
import { useControls } from 'leva'

function Box() {
  const [ref] = useBox(() => ({ mass: 1, position: [0, 1, 1] }));
  return (
    <mesh
      ref={ref}
      position={[0, 0, 0]}
    >
      <boxGeometry attach="geometry" args={[1, 1]} />
      <meshLambertMaterial attach="material" color="hotpink" />
    </mesh>
  );
}



const App = () => {
  const displacementMap = useLoader(
    TextureLoader,
    "FrDOO.jpg"
  )

  // const material = useControls({
  //   wireframe: false,
  //   displacementScale: { value: 0.5, min: 0, max: 1.0, step: 0.01 }
  // })

  function Plane() {
    const [ref] = usePlane(() => ({
      mass: 0,
      rotation: [-Math.PI / 2, 0, 0],
    }));
    return (
      <mesh ref={ref} rotation={[-Math.PI / 2, 0, 0]}>
        <planeGeometry attach="geometry" args={[5, 5, 100, 100]} />
        <meshStandardMaterial
          attach="material" color="lightblue" 
          displacementMap={displacementMap} 
          wireframe={true} />
      </mesh>
    );
  }

  return (
    <Canvas shadows camera={{ position: [0, 0, 1.75] }}>
      <OrbitControls />
      <Stars />
      <ambientLight intensity={0.5} />
      <spotLight position={[10, 15, 10]} angle={0.3} />
      <Physics>
        <Plane />
        <Box />
      </Physics>
    </Canvas>
  )
}

export default App
